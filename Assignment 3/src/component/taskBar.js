import { useState } from "react";
import CheckButton from "../common/checkButton";
import RemoveButton from "../common/removeButton";

const TaskBar = () => {
  const [input, setInput] = useState('');
  const [tasks, setTasks] = useState([]);
  const [completedTasks, SetCompletedTasks] = useState([]);

  const addTask = () =>{
    if(input.length > 0){
      setTasks(values => ([...values, input]));
    }
    setInput('');
  }
  const handleTaskRemove = (index) =>{
    const updatedItems = [...tasks];
    updatedItems.splice(index, 1);
    setTasks(updatedItems);
  }
  const handleTaskComplete = (task, index) =>{
    SetCompletedTasks(values => ([...values, task]));
    handleTaskRemove(index);
  }
  return(
    <>
      <div className="conatiner">
        <div className="input-container">
          <input type="text" className="addTask" value={input} onChange={(e) => setInput(e.target.value)} placeholder="Add task here..."/>
          <span>
            <i className="fas fa-plus" onClick={() =>{addTask()}}></i>
          </span>
        </div>
        <div className={`task-list-container ${(completedTasks.length > 0 && tasks.length > 0) && 'border-2'}`}>
          {
            tasks.map((task, index) => (
              <div className="task-container">
                <div className="padding-l5">
                  {
                    task 
                  }
                </div>
                <span>
                  <CheckButton style={{color : "green", fontSize : '20px'}} handleClick={() => handleTaskComplete(task, index)}/>
                  <RemoveButton style={{color : "red", fontSize : '20px'}} handleClick={() => handleTaskRemove(index)}/>
                </span>
              </div>
            ))
          }
        </div>
        <div>
          {
            completedTasks.length > 0 && 
            <div>
              {
                completedTasks.map((completedTask) => (
                  <div className="input-container completedtask">
                    <span>{completedTask}</span>
                    <CheckButton style={{color : "green", fontSize : '20px'}}/>
                  </div>
                ))
              }
            </div>
          }
        </div>
      </div>
    </>
  );
}

export default TaskBar