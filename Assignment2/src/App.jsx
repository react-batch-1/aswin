import React from "react";
import DataView from "./components/DataView/DataView";
import Navbar from "./components/Navbar";
import "./App.css";

const App = () => {
  return (
    <div className="App">
      <Navbar />
      <h1 className="header">Earn More, Pay Less? </h1>
      <h2 className="subheader">
        Average Housing Costs and Incomes by NYC Borough
      </h2>
      <DataView 
        borough="Staten Island"
        renterCost='$14292'
        ownerCost='$28752'
        renterIncome='$37882'
        ownerIncome='$94177' 
      />
    </div>
  );
};

export default App;
