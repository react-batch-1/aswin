import React, { Component } from 'react';
import './App.css';
import Parent from './component/parent';

class App extends Component{
 
  render() { 
    return ( 
      <div> 
        <Parent />
      </div> 
    ); 
  } 
}

export default App;
