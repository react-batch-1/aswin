import { Component } from "react";

class Child extends Component{

  render(){
    return(
      <div>
        {this.props.message}
      </div>
    );
  }

}

export default Child;