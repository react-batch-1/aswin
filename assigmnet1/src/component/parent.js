import { Component } from "react";
import Child from "./child";

class Parent extends Component{
  constructor(props){
    super(props);
    this.data = "Hello from Parent";
  }
  render(){
    return(
      <div>
        <Child message = {this.data} />
      </div>
    );
  }

}

export default Parent;