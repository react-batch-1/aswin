import './App.css';
import TaskBar from './component/taskBar';

function App() {
  return (
   <>
    <TaskBar />
   </>
  );
}

export default App;
