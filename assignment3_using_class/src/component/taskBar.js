import { Component } from "react";
import CheckButton from "../common/checkButton";
import RemoveButton from "../common/removeButton";

class TaskBar extends Component{
  constructor(){
    super();
    this.state = {
      input : '',
      tasks : [],
      completedTasks : [],
      showLoadAnimation : true,
      componentUpdated : false
    }
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState(values => ({...values, showLoadAnimation : false}));
    }, 2000)
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(prevState);
    if(this.state.completedTasks.length !== prevState.completedTasks.length || this.state.tasks.length !== prevState.tasks.length){
      this.setState(values => ({...values, componentUpdated : true}));
    }
  }

  addTask(){
    if(this.state.input.length > 0){
      var UpdatedTasks = [...this.state.tasks, this.state.input];
      this.setState(values => ({...values, tasks : UpdatedTasks}));
    }
    this.setState(values => ({...values, input : ''}));
  }
  handleTaskRemove(index){
    const updatedItems = [...this.state.tasks];
    updatedItems.splice(index, 1);
    this.setState(values => ({...values, tasks : updatedItems}));
  }
  handleTaskComplete(task, index){
    var updatedCompletedTaks = [...this.state.completedTasks, task];
    this.setState(values => ({...values, completedTasks : updatedCompletedTaks}))
    this.handleTaskRemove(index);
  }
  render(){
    return(
      <>
        <div className="conatiner">
          {
            this.state.componentUpdated &&
              <div className="width-100 text-right">
                <span>Component Updated</span>
              </div>
          }
          <div className="input-container">
            <input type="text" className="addTask" value={this.state.input} onChange={(e) => this.setState(values => ({...values, input : e.target.value}))} placeholder="Add task here..."/>
            <span>
              <i className="fas fa-plus" onClick={() =>this.addTask()}></i>
            </span>
          </div>
          <div className={`task-list-container ${(this.state.completedTasks.length > 0 && this.state.tasks.length > 0) && 'border-2'}`}>
            {
              this.state.tasks.map((task, index) => (
                <div className="task-container">
                  <div className="padding-l5">
                    {
                      task 
                    }
                  </div>
                  <span>
                    <CheckButton style={{color : "green", fontSize : '20px'}} handleClick={() => this.handleTaskComplete(task, index)}/>
                    <RemoveButton style={{color : "red", fontSize : '20px'}} handleClick={() => this.handleTaskRemove(index)}/>
                  </span>
                </div>
              ))
            }
          </div>
          <div>
            {
              this.state.completedTasks.length > 0 && 
              <div>
                {
                  this.state.completedTasks.map((completedTask) => (
                    <div className="input-container completedtask">
                      <span>{completedTask}</span>
                      <CheckButton style={{color : "green", fontSize : '20px'}}/>
                    </div>
                  ))
                }
              </div>
            }
          </div>
            {
              this.state.showLoadAnimation &&
                <div class="page_load_animation center">  
                  <div class="wave"></div>
                  <div class="wave"></div>
                  <div class="wave"></div>
                  <div class="wave"></div>
                  <div class="wave"></div>
                  <div class="wave"></div>
                  <div class="wave"></div>
                  <div class="wave"></div>
                  <div class="wave"></div>
                  <div class="wave"></div>
                </div>
            }
        </div>
      </>
    );
  };
}

export default TaskBar