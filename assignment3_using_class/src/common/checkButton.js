const CheckButton = (
  props
) =>{
  return(
    <>
      <i class="fa-solid fa-circle-check" style={props.style} onClick={props.handleClick}></i>
    </>
  );
}

export default CheckButton;