const RemoveButton = (
  props
) =>{
  return(
    <>
      <i class="fa-solid fa-circle-xmark" style={props.style} onClick={props.handleClick}></i>
    </>
  );
}

export default RemoveButton;